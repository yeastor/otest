<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09.03.2017
 * Time: 21:48
 */
require_once('FileException.php');
require_once('PhpExcel/Classes/PHPExcel.php');

include('header.html');




try {
    if (array_key_exists('userfile',$_FILES)){
        $file = $_FILES['userfile'];
        $uploadTo = '';
        $fileName = $file['name'];
        $fileTmpName = $file['tmp_name'];
        $uploadFileName = $uploadTo . basename($fileName);
        processFile($fileTmpName, $uploadFileName);
    }

} catch (FileException $e) {

    echo $e->getMessage();

} catch (Exception $e) {

    echo "something else went wrong";

} finally {

    if (file_exists($uploadFileName)) {
        unlink($uploadFileName);
    }
}


include('form.html');


function processFile($tmpFileName, $uploadFileName)
{

    $neededExt = "xlsx";
    preg_match("/\.([\w]{2,4})/", $uploadFileName, $ext);
    if ($ext[1] != $neededExt) throw new FileException("wrong file extension");

    if (!move_uploaded_file($tmpFileName, $uploadFileName)) {
        throw new FileException("cannt upload file");
    }


    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
    $objPHPExcel = $objReader->load($uploadFileName);

    if (!checkFileSheets($objPHPExcel->getSheetNames())) {
        throw new FileException("wrong sheets name in file");
    }
    // ��������� �� ��������
    $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
    $users = array();
    foreach ($objWorksheet->getRowIterator(1) as $row) {
        $user = array();
        foreach ($row->getCellIterator() as $cell) {
            $user[] = $cell->getFormattedValue();

        }
        if (!checkUser($user)) {
            throw new FileException("wrong client data on sheet first, row " . $row->getRowIndex());
        }
        $users[$user[0]] = $user;
    }

    // ��������� �� ����/�����
    $objWorksheet = $objPHPExcel->setActiveSheetIndex(1);
    foreach ($objWorksheet->getRowIterator(1) as $row) {

        $id = $objWorksheet->getCellByColumnAndRow(0, $row->getRowIndex())->getFormattedValue();
        $IOValue = $objWorksheet->getCellByColumnAndRow(1, $row->getRowIndex())->getFormattedValue();

        if (!checkUserData([$id, $IOValue])) {
            throw new FileException("wrong client data on on sheets second, row " . $row->getRowIndex());
        }
        //�.�. users �� ��� ��������� ������� �������,
        //��������� ����� �� ���������, � ����� ����������� �� ����� �����/������
        $users[$id][2] += $IOValue;
    }

    foreach ($users as $user) {
        echo $user[1] . " summ: " . $user[2] . "<br/>";
    }

}

/**
 * @param array $sheets
 * @return bool
 */
function checkFileSheets(array $sheets)
{
    $neededSheets = ['first', 'second'];
    return ($neededSheets[0] == $sheets[0] && $neededSheets[1] == $sheets[1]);
};

/**
 * @param array $user
 * @return bool
 */
function checkUser(array $user)
{

    if (count($user) != 3) return false;
    if (!is_numeric($user[0])) return false;
    if (!preg_match("/^[�-�][�-�]+\s*([�-�]\.)*/", iconv('utf-8', 'cp1251', $user[1]))) return false;
    if (!is_numeric($user[2])) return false;

    return true;
};

/**\
 * @param array $userData
 * @return bool
 */
function checkUserData(array $userData)
{
    if (count($userData) != 2) return false;
    if (!is_numeric($userData[0])) return false;
    if (!is_numeric($userData[1])) return false;

    return true;
}